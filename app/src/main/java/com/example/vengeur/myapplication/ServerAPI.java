package com.example.vengeur.myapplication;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by vengeur on 8/17/16.
 */
public class ServerAPI {
    //Конфигурация сервера
    static String server = "vengeur.h1n.ru";
    static String token = null;







    static boolean reg_user(String login, String pwd) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://" + server + "/api.php?action=GetToken");

            Log.d("http", "http://" + server + "/api.php?action=GetToken");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("login", login));
            pairs.add(new BasicNameValuePair("password", pwd));
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse response = client.execute(httpPost);
            String responseStr = EntityUtils.toString(response.getEntity());
            Log.d("http ---- GetToken", responseStr);
            if (responseStr.equalsIgnoreCase("0")) {
                return false;
            } else {
                token = responseStr;
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /// 1- block
    /// 2 - unblock
    /// 3 - error
    static int GetStatus(String token) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://" + server + "/api.php?action=GetStatus");

            Log.d("http", "http://" + server + "/api.php?action=GetStatus");
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("token", token));
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse response = client.execute(httpPost);
            String responseStr = EntityUtils.toString(response.getEntity());
            Log.d("http", responseStr);
            if (responseStr.equalsIgnoreCase("1")) {
                return 1;
            }
            if (responseStr.equalsIgnoreCase("2")) {
                return 2;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return 3;
        }
        return 0;

    }

}
