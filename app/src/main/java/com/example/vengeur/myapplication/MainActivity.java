
package com.example.vengeur.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class MainActivity extends Activity {


    Button btStart;
    EditText eT_login;
    EditText eT_pass;
    boolean user_is_reg = false;

    protected String login, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btStart = (Button)findViewById(R.id.button);
        eT_login = (EditText) findViewById(R.id.eT_login);
        eT_pass = (EditText) findViewById(R.id.eT_pass);
        btStart.setOnClickListener(ocl_btStart);
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        if (pref.getBoolean("active", false)){
            btStart.setEnabled(false);
            eT_login.setEnabled(false);
            eT_pass.setEnabled(false);
            startService(new Intent(MainActivity.this, LockService.class));
            finish();
        }
    }

    //Проверка
    OnClickListener ocl_btStart = new OnClickListener() {
        @Override
        public void onClick(View v) {

            if (eT_login.getText().length() < 4){
                eT_login.requestFocus();
                Toast.makeText(getApplicationContext(),"Логин < 4 символов",
                        Toast.LENGTH_SHORT).show();
                return;

            }
            if (eT_pass.getText().length() < 4){
                eT_pass.requestFocus();
                Toast.makeText(getApplicationContext(),"Пароль < 4 символов",
                        Toast.LENGTH_SHORT).show();
                return;

            }

            Req r = new Req();
            r.execute();
        }
    };

    class Req extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btStart.setEnabled(false);
            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            eT_login.setEnabled(false);
            eT_pass.setEnabled(false);
            login = eT_login.getText().toString();
            password = eT_pass.getText().toString();

        }

        @Override
        protected Void doInBackground(Void... params) {
            user_is_reg = ServerAPI.reg_user(login,password);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
            if (user_is_reg){
            btStart.setText("True");
                startService(new Intent(MainActivity.this, LockService.class));
                SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("active", true);
                editor.putString("token", ServerAPI.token);
                Log.d("service", "Token = " + ServerAPI.token);
                editor.commit();

            }
            else{
                btStart.setEnabled(true);
                eT_login.setEnabled(true);
                eT_pass.setEnabled(true);
                if (ServerAPI.token.equalsIgnoreCase("0")) {
                    Toast.makeText(getApplicationContext(), "Ни верный пароль",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Ошибка подключения",
                            Toast.LENGTH_SHORT).show();
                }

            }
        }
    }


}
