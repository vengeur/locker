package com.example.vengeur.myapplication;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class LockService extends Service {


    static Intent main_intent;
    private int status;
    public LockService() {
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Служба создана",
                Toast.LENGTH_SHORT).show();
    }

    Runnable runn1 = new Runnable() {
        public void run() {
            Toast.makeText(null, "Служба работает",
                    Toast.LENGTH_SHORT).show();
        }
    };
    class UpdateTimeBlockTask extends TimerTask {
        public void run() {
            SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
            if (pref.getBoolean("active", false)) {
                if (status == 1) {
                    Intent dialogIntent = new Intent(getBaseContext(), Lock.class);
                    main_intent = dialogIntent;
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(dialogIntent);
                }
                if (status == 2) {
                    Lock.Close_past();
                }
            }
        }
    }
    class UpdateTimeCheckTask extends TimerTask {
        public void run() {
            Log.d("service", "Check status");
            SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
            if (pref.getBoolean("active", false)){
                int stat = ServerAPI.GetStatus(pref.getString("token", "error"));
                if (stat == 1) {
                    status = 1;
                    Log.d("service", "Check status -> Block");
                }
                if (stat == 2) {
                    status = 2;
                    Log.d("service", "Check status -> Unblock");
                }
                if (stat == 3) {
                    Log.d("service", "Check status -> Error");
                }
            }
        }
    }
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        main_intent = intent;
        Timer timerBlock = new Timer();
                timerBlock.schedule(new UpdateTimeBlockTask(), 0, 1000);

        Timer timerCheck = new Timer();
        timerCheck.schedule(new UpdateTimeCheckTask(), 0, 20000);
        return START_STICKY;
        }
        @Override
        public IBinder onBind (Intent intent){
            // TODO: Return the communication channel to the service.
            throw new UnsupportedOperationException("Not yet implemented");
        }

}
